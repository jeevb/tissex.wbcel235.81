# Tissue Expression Predictions for *Caenorhabditis elegans*
### Compatible with Ensembl WBcel235 Release 81

Original data sourced from [http://worm-tissue.princeton.edu/](http://worm-tissue.princeton.edu/), and gene names cleaned up to match Ensembl IDs. Additional alternate names to facilitate nomenclature conversion were sourced from Wormbase Release 249.

##### Usage:
```
# Install package
devtools::install_bitbucket('jeevb/TissEx.WBcel235.81')

# Load library
library(TissEx.WBcel235.81)

# Source dataset
data(gold.standard)
```
